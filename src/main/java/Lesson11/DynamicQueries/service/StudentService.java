package Lesson11.DynamicQueries.service;

import Lesson11.DynamicQueries.dto.SearchCriteria;
import Lesson11.DynamicQueries.dto.StudentSpecification;
import Lesson11.DynamicQueries.model.Student;
import Lesson11.DynamicQueries.repository.StudentRepository;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Predicate;
import jakarta.persistence.criteria.Root;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepository studentRepository;

    public List<Student> getAllBy(){
        return studentRepository.findAll(ageGreaterThan(20));
    }

    public List<Student> getAllByName(){
        return studentRepository.findAll(Specification.where(nameLike("Ali"))
                .and(gpaLessThan(50.0)));}

    public List<Student> getAllByCriteria(List<SearchCriteria> dto) {
        StudentSpecification studentSpecification=new StudentSpecification();
        dto.forEach(searchCriteria -> studentSpecification.add(searchCriteria));
        return studentRepository.findAll(studentSpecification);
    }

    private Specification<Student> ageGreaterThan(int age){
        return (root,query,criteriaBuilder)->criteriaBuilder.greaterThan(root.get(Student.Fields.age),age);
    }

    private Specification<Student> nameLike(String name){
        return (root,query,criteriaBuilder)->criteriaBuilder.like(root.get(Student.Fields.name),name+"%");
    }

    private Specification<Student> gpaLessThan(double gpa){
        return (root,query,criteriaBuilder)->criteriaBuilder.lessThan(root.get(Student.Fields.gpa),gpa);
    }



//    private Specification<Student> ageGreaterThan(int age){
//        return new Specification<Student>() {
//            @Override
//            public Predicate toPredicate(Root<Student> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
//                return criteriaBuilder.greaterThan(root.get(Student.Fields.age),age);
//            }
//        };
//    }


}
