package Lesson11.DynamicQueries;

import Lesson11.DynamicQueries.model.Student;
import Lesson11.DynamicQueries.repository.StudentRepository;
import Lesson11.DynamicQueries.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
public class DynamicQueriesApplication implements CommandLineRunner {

	private final StudentRepository studentRepository;

	private final StudentService studentService;

	public static void main(String[] args) {
		SpringApplication.run(DynamicQueriesApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

//		studentService.getAllByName().forEach(System.out::println);

//		for(int i=0;i<50;i++){
//			Student build = Student.builder()
//					.name("Ali" + i)
//					.surname("Mammadov" + i)
//					.age(18)
//					.gpa(44.5+i)
//					.address("Baku")
//					.build();
//			studentRepository.save(build);
//		}
//
//		for(int i=51;i<100;i++){
//			Student build = Student.builder()
//					.name("Zaur" + i)
//					.surname("Eliyev" + i)
//					.age(getRandomNumber(20,30))
//					.gpa(5.5+i)
//					.address("London")
//					.build();
//			studentRepository.save(build);
//		}
	}

	public int getRandomNumber(int min,int max){
		return (int)((Math.random()*(max-min)+min));
	}
}
