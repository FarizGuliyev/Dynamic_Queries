package Lesson11.DynamicQueries.controller;

import Lesson11.DynamicQueries.dto.SearchCriteria;
import Lesson11.DynamicQueries.model.Student;
import Lesson11.DynamicQueries.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping
@RequiredArgsConstructor
public class StudentController {
    private final StudentService studentService;

    @GetMapping("/list")
    public List<Student> getListByCriteria(@RequestBody List<SearchCriteria> dto){
        return studentService.getAllByCriteria(dto);
    }
}
